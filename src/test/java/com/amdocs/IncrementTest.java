package com.amdocs;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.junit.Test;
import org.mockito.Mockito;

public class IncrementTest extends Mockito{

    @Test
    public void testIncrement() throws Exception {
        assertTrue(true);
    }

    @Test
    public void testCounter() throws Exception {

        int k= new Increment().getCounter();
        assertEquals("Addition check failed",k,3);
        
    }

    @Test 
    public void testdecreasecounter() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Subtract check failed 1",k,4);
		
		int l= new Increment().decreasecounter(1);
        assertEquals("Subtract check failed 2",l,3);
		
		int m= new Increment().decreasecounter(1);
        assertEquals("Subtract check failed 3",m,3);

}    }
